"use strict";

// порожній об'єкт
const student = {
  name: prompt("Ваше Імʼя"),
  lastName: prompt("Ваше прізвище"),
  table: {},
};

// цикл назви предмета та оцінки з предмету
while (true) {
  let subjectName = prompt("Назва предмету");
  console.log(subjectName);
  if (subjectName === null) {
    break;
  }
  let assessment = prompt("Оцінка з предмету");
  console.log(assessment);
  if (assessment === null) {
    break;
  }
  student.table[subjectName] = Number(assessment);
}

// кількість поганих оцінок з предметів
let badGrades = 0;
for (let subject in student.table) {
  if (student.table[subject] < 4) {
    badGrades++;
  }
}
if (badGrades === 0) {
  console.log("Студент переведено на наступний курс");
} else {
  console.log(`Кількість поганих оцінок: ${badGrades}`);
}

// середній бал з предметів
let assessmentSum = 0;
let assessmentCount = 0;
for (let subject in student.table) {
  assessmentSum += student.table[subject];
  assessmentCount++;
}
let assessmentAverage = assessmentSum / assessmentCount;
if (assessmentAverage > 7) {
  console.log("Студенту призначено стипендію");
}
